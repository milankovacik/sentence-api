#!/bin/sh

psql -U admin -d sentense_db -a -f down.sql

psql postgres << END_OF_SCRIPT

DROP DATABASE sentense_db; -- drop the DB

DROP USER admin;

CREATE ROLE admin WITH LOGIN PASSWORD 'Xasdf54asd' SUPERUSER;


CREATE DATABASE sentense_db OWNER admin;

\c sentense_db

-- Create some schemas on the DB

CREATE SCHEMA public AUTHORIZATION admin;


END_OF_SCRIPT

psql -U admin -d sentense_db -a -f init.sql