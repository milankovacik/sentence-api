CREATE TABLE word(
 id serial PRIMARY KEY,
 word VARCHAR (50) UNIQUE NOT NULL,
 type SMALLINT NOT NULL
);

 ALTER TABLE public.word OWNER TO admin;

 CREATE TABLE sentence(
 id serial PRIMARY KEY,
 created TIMESTAMP NOT NULL,
 count_of_view SMALLINT NOT NULL
);

 ALTER TABLE public.sentence OWNER TO admin;


 CREATE TABLE sentence_word(
 word_id INTEGER NOT NULL,
 sentence_id INTEGER NOT NULL,
 word_order SMALLINT NOT NULL,
 PRIMARY KEY (word_id, sentence_id, word_order),
  CONSTRAINT sentence_word_word_id_fkey FOREIGN KEY (word_id)
      REFERENCES word (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT sentence_word_sentence_id_fkey FOREIGN KEY (sentence_id)
      REFERENCES sentence (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

  ALTER TABLE public.sentence_word OWNER TO admin;

CREATE TABLE sentence_duplicates(
 sentence_id INTEGER NOT NULL,
 date_time TIMESTAMP NOT NULL,
 PRIMARY KEY (sentence_id, date_time),
 CONSTRAINT sentence_duplicates_sentence_id_fkey FOREIGN KEY (sentence_id)
      REFERENCES sentence (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

