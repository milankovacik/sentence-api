package com.milanKovacik.sentences.builder;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.milanKovacik.sentences.entity.Sentence;
import com.milanKovacik.sentences.entity.Word;
import com.milanKovacik.sentences.entity.enums.WordType;

/**
 * 
 * Build {@link Sentence} form {@link List} of {@link Word} by settings 
 * @author milan
 *
 */
public interface SentencesBuilder {

	/**
	 * Define order of word types in sentence
	 * @return
	 */
	TreeMap<Integer, WordType> getWordTypeMap();
	
	/**
	 * From list of {@link Word} create map with key {@link WordType} and list of {@link Word} with same {@link WordType}  
	 * When for some {@link WordType} doesn't exit any {@link Word} in wordList throw {@link IllegalArgumentException}
	 * @param wordList
	 * @return
	 */
	default Map<WordType, List<Word>> getWordMap(List<Word> wordList) {
		Map<WordType, List<Word>> map = new HashMap<>();
		for (WordType type : getWordTypeMap().values()) {
			if(map.get(type) == null)
				map.put(type, wordList.stream().filter(w-> w.getType() == type).collect(Collectors.toList()));
			if(map.get(type).isEmpty())
				throw new IllegalArgumentException("There is no word with type: " + type);
		}
		return map;
	}
	
	/**
	 * Create sentence with order list of {@link Word}
	 * @param wordList
	 * @return
	 */
	default Sentence create(List<Word> wordList) {
		LinkedList<Word> result = new LinkedList<>(); 
		Map<WordType, List<Word>> wordMap = getWordMap(wordList);
		Random rand = new Random();
		for (WordType type : getWordTypeMap().values()) {
			List<Word> wordListByType = wordMap.get(type);
			result.add(wordListByType.get(rand.nextInt(wordListByType.size())));
		}
		return new Sentence(result);
	}
	
	/**
	 * Render String from wordList
	 * @param wordList
	 * @return
	 */
	default String createString(List<Word> wordList) {
		Sentence sentence = create(wordList);
		return sentence.getWordList().stream().map(Word::getWord).collect(Collectors.joining(" ")) + ".";
	}
}
