package com.milanKovacik.sentences.builder;

import java.util.TreeMap;

import com.milanKovacik.sentences.entity.enums.WordType;

public class YodaSentenceBulider implements SentencesBuilder {

	@Override
	public TreeMap<Integer, WordType> getWordTypeMap() {
		TreeMap<Integer, WordType> map = new TreeMap<>();
		map.put(1, WordType.NOUN);
		map.put(2, WordType.ADJECTIVE);
		map.put(3, WordType.VERB);
		return map;
	}
}
