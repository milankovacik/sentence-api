package com.milanKovacik.sentences.config;

import java.nio.file.AccessDeniedException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.AllArgsConstructor;
import lombok.Data;

@ControllerAdvice
public class RestApiExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
	protected ResponseEntity<Error> handleConflict(RuntimeException ex, WebRequest request) {
		return new ResponseEntity<Error>(new Error("Illegal state: " + ex.getMessage()), new HttpHeaders(), HttpStatus.CONFLICT);
	}

	@ExceptionHandler({ AccessDeniedException.class })
	public ResponseEntity<Error> handleAccessDeniedException(Exception ex, WebRequest request) {
		return new ResponseEntity<Error>(new Error("Access denied."), new HttpHeaders(), HttpStatus.FORBIDDEN);
	}
	
	@ExceptionHandler({ NullPointerException.class })
	public ResponseEntity<Error> handleNullPointerException(Exception ex, WebRequest request) {
		return new ResponseEntity<Error>(new Error("Bad request."), new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}
	
	@Data
	@AllArgsConstructor
	private class Error {
		private String errorMsg;
	}
}
