package com.milanKovacik.sentences.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.milanKovacik.sentences")
@MapperScan("com.milanKovacik.sentences.dao")
public class SentencesApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SentencesApiApplication.class, args);
	}
}
