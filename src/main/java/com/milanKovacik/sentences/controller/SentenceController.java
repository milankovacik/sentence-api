package com.milanKovacik.sentences.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.milanKovacik.sentences.entity.Sentence;
import com.milanKovacik.sentences.entity.enums.SenteceType;
import com.milanKovacik.sentences.service.SentenceService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/sentences")
public class SentenceController {

	@Autowired
	private SentenceService sentenceService;

	@RequestMapping( method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Sentence>> getAllSentences() {
		return ResponseEntity.ok(sentenceService.selectAll());
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Sentence> getSentence(@PathVariable(value = "id", required = true) int id) {
		sentenceService.updateCountOfView(id);
		return ResponseEntity.ok(sentenceService.select(id));
	}

	@RequestMapping(value = "/{id}/yodaTalk", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Sentence> getYodaTalk(@PathVariable(value = "id", required = true) int id) {
		sentenceService.updateCountOfView(id);
		return ResponseEntity.ok(sentenceService.select(id).withType(SenteceType.YODA));
	}

	@RequestMapping(value ="generate", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Sentence> generate() {
		Sentence sentence = sentenceService.generate();
		return new ResponseEntity<Sentence>(sentence, HttpStatus.CREATED);
	}
}
