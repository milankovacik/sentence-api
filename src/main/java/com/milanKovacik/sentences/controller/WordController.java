package com.milanKovacik.sentences.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.milanKovacik.sentences.entity.Word;
import com.milanKovacik.sentences.service.WordService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/words")
public class WordController {
	
	@Autowired
	private WordService wordService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Word>> getAll() {
		return ResponseEntity.ok(wordService.selectAll());
	}
	
	@RequestMapping(value = "/{word}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Word> getWord(@PathVariable( value = "word", required = true) String word) {
		return ResponseEntity.ok(wordService.selectByWord(word));
	}
	
	@RequestMapping(value = "/{word}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Word> insert(@PathVariable( value = "word", required = true) String wordStr, @RequestBody Word word) {
		word.setWord(wordStr);
		wordService.insert(word);
		return ResponseEntity.ok(word);
	}
	

}
