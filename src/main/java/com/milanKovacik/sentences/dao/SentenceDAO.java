package com.milanKovacik.sentences.dao;

import java.util.List;

import com.milanKovacik.sentences.entity.Sentence;

public interface SentenceDAO {

	Sentence select(int id);

	void insert(Sentence sentence);

	void updateCountOfView(int id);

	List<Sentence> selectAll();

	Sentence selectDuplicate(Sentence sentence);
}
