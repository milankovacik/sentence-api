package com.milanKovacik.sentences.dao;

import java.util.List;

import com.milanKovacik.sentences.entity.Word;

public interface WordDAO {

	Word select(int id);
	
	void insert(Word word);

	Word selectByWord(String word);

	List<Word> selectAll();
}
