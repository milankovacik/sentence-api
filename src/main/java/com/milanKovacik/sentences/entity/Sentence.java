package com.milanKovacik.sentences.entity;

import java.util.LinkedList;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.milanKovacik.sentences.builder.NormalSentenceBulider;
import com.milanKovacik.sentences.builder.YodaSentenceBulider;
import com.milanKovacik.sentences.config.JsonJodaDateTimeSerializer;
import com.milanKovacik.sentences.entity.enums.SenteceType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Sentence {

	private Integer id;
	private int showDisplayCount = 0;
	@JsonSerialize(using = JsonJodaDateTimeSerializer.class)
	private DateTime created;
	@JsonIgnore
	private LinkedList<Word> wordList;
	@JsonIgnore
	private SenteceType type = SenteceType.NORMAl;
	private int duplicates;

	public Sentence(LinkedList<Word> wordList) {
		this.wordList = wordList;
	}

	public Sentence withType(SenteceType type) {
		this.type = type;
		return this;
	}
	
	public String getText() {
		if (type == SenteceType.YODA)
			return new YodaSentenceBulider().createString(wordList);
		return new NormalSentenceBulider().createString(wordList);
	}

}
