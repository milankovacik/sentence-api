package com.milanKovacik.sentences.entity;

import com.milanKovacik.sentences.entity.enums.WordType;

import lombok.Data;

@Data
public class Word {

	private Integer id;
	private String word;
	private WordType type;
	
}
