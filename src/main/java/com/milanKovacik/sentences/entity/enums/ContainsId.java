package com.milanKovacik.sentences.entity.enums;

import com.milanKovacik.sentences.typeHandler.AbstractTypeHandler;

/**
 * When this interface implements enum and can be type to db must have method name valueOf(Number number) 
 * @see AbstractTypeHandler#getResult(String code)
 * @author milan
 *
 * @param <T>
 */
public interface ContainsId<T extends Number> {

	T getId();	
}

