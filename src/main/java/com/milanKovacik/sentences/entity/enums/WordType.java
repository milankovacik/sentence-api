package com.milanKovacik.sentences.entity.enums;

public enum WordType implements ContainsId<Integer> {
	NOUN(1), VERB(2), ADJECTIVE(3);

	private int id;

	private WordType(int id) {
		this.id = id;
	}

	@Override
	public Integer getId() {
		return id;
	}

	public static WordType valueOf(Number n) {
		for (WordType type : values()) {
			if (type.getId() == n.intValue())
				return type;
		}
		throw new IllegalArgumentException("Invalid id: " + n.intValue());
	}
}
