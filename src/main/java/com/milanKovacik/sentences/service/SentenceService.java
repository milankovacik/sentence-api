package com.milanKovacik.sentences.service;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.milanKovacik.sentences.builder.NormalSentenceBulider;
import com.milanKovacik.sentences.dao.SentenceDAO;
import com.milanKovacik.sentences.dao.SentenceDuplicatesDAO;
import com.milanKovacik.sentences.entity.Sentence;
import com.milanKovacik.sentences.entity.Word;

@Service
public class SentenceService {

	private final SentenceDAO sentenceDAO;
	private final WordService wordService;
	private final SentenceDuplicatesDAO sentenceDuplicatesDAO;

	@Autowired
	private SentenceService(SentenceDAO sentenceDAO, WordService wordService, SentenceDuplicatesDAO sentenceDuplicatesDAO) {
		this.sentenceDAO = sentenceDAO;
		this.wordService = wordService;
		this.sentenceDuplicatesDAO = sentenceDuplicatesDAO;
	}

	public void insert(Sentence sentence) {
		sentenceDAO.insert(sentence);
	}

	public Sentence select(int id) {
		return sentenceDAO.select(id);
	}

	public void updateCountOfView(int id) {
		sentenceDAO.updateCountOfView(id);
	}

	public List<Sentence> selectAll() {
		return sentenceDAO.selectAll();
	}

	public Sentence generate() {
		List<Word> wordList = wordService.selectAll();
		NormalSentenceBulider builder = new NormalSentenceBulider();
		Sentence sentence = builder.create(wordList);
		Sentence duplicate = sentenceDAO.selectDuplicate(sentence);
		if(duplicate != null) {
			sentenceDuplicatesDAO.insert(duplicate.getId());
			return duplicate;	
		} else {			
			sentence.setCreated(new DateTime());
			insert(sentence);
		}
		return sentence;
	}

}
