package com.milanKovacik.sentences.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.milanKovacik.sentences.dao.WordDAO;
import com.milanKovacik.sentences.entity.Word;

@Service
public class WordService {

	private static final String FORBIDDEN_PATH = "files/forbidden.txt";

	private final WordDAO wordDAO;

	@Autowired
	private WordService(WordDAO wordDAO) {
		this.wordDAO = wordDAO;
	}

	public void insert(Word word) {
		checkForbiddenWord(word);
		if(selectByWord(word.getWord()) != null)
			throw new IllegalArgumentException("Word: " + word.getWord() + " already exist.");
		this.wordDAO.insert(word);
	}

	private void checkForbiddenWord(Word word) {
		Resource resource = new ClassPathResource(FORBIDDEN_PATH);
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream(), "UTF-8"))) {
			if (reader.lines().anyMatch(line -> line.equals(word.getWord())))
				throw new IllegalArgumentException("Forbidden word");
		} catch (IOException e) {
			throw new IllegalArgumentException("Exception during check forbidden word. Reason: " + e.getMessage(), e);
		}
	}

	public Word select(int id) {
		return wordDAO.select(id);
	}

	public Word selectByWord(String word) {
		return wordDAO.selectByWord(word);
	}

	public List<Word> selectAll() {
		return wordDAO.selectAll();
	}

}
