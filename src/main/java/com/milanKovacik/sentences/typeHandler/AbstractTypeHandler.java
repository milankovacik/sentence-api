package com.milanKovacik.sentences.typeHandler;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.milanKovacik.sentences.entity.enums.ContainsId;

public abstract class AbstractTypeHandler<T extends ContainsId<?>> extends BaseTypeHandler<T> {

	protected Class<?> getClazz() {
		return (Class<?>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, T parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i,  parameter.getId().intValue());
	}

	@Override
	public T getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String code = rs.getString(columnName);
		return getResult(code);

	}

	@Override
	public T getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		String code = rs.getString(columnIndex);
		return getResult(code);
	}

	@Override
	public T getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String code = cs.getString(columnIndex);
		return getResult(code);
	}

	private T getResult(String code) {
		if (StringUtils.isBlank(code))
			return null;
		try {
			Method method = getClazz().getMethod("valueOf", Number.class);
			Number n = NumberFormat.getInstance().parse(code);
			return (T) method.invoke(null, n);
		} catch (Exception e) {
			throw new IllegalArgumentException("Bad type of enum for class: " + getClazz() + " code: " + code, e);
		}

	}
}
