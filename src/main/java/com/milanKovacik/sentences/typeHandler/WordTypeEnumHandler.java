package com.milanKovacik.sentences.typeHandler;

import org.apache.ibatis.type.MappedTypes;

import com.milanKovacik.sentences.entity.enums.WordType;

@MappedTypes(WordType.class)
public class WordTypeEnumHandler extends AbstractTypeHandler<WordType> {
}
