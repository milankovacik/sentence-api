package com.milanKovacik.sentences;

import static org.hamcrest.CoreMatchers.startsWith;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.hamcrest.text.IsEmptyString;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.milanKovacik.sentences.config.SentencesApiApplication;
import com.milanKovacik.sentences.dao.SentenceDAO;
import com.milanKovacik.sentences.dao.WordDAO;
import com.milanKovacik.sentences.entity.Sentence;
import com.milanKovacik.sentences.entity.Word;
import com.milanKovacik.sentences.entity.enums.WordType;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SentencesApiApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Slf4j
public class SentencesApiApplicationTests {

	private static final DateTime CREATED = new DateTime();
	
	private MockMvc mvc;

	@Autowired
	private WebApplicationContext wac;

	@MockBean
	private WordDAO wordDAO;
	
	@MockBean
	private SentenceDAO sentenceDAO;

	@Before
	public void setup() {
		this.mvc = MockMvcBuilders.webAppContextSetup(wac).build();
		
		Word noun1 = createWord(10, "aaa", WordType.NOUN);
		Word adj1 = createWord(11, "adj", WordType.ADJECTIVE);
		Word verb1 = createWord(12, "verb", WordType.VERB);
		
		Word noun2 = createWord(10, "NOUN", WordType.NOUN);
		Word adj2 = createWord(11, "ADJECTIVE", WordType.ADJECTIVE);
		Word verb2 = createWord(12, "VERB", WordType.VERB);
	
		List<Word> completeWordList = Stream.of(noun1, adj1, verb1).collect(Collectors.toList());
		List<Word> completeWordList2 = Stream.of(noun2, adj2, verb2).collect(Collectors.toList());

		when(wordDAO.selectByWord("aaa")).thenReturn(noun1);
		when(wordDAO.selectAll()).thenReturn(completeWordList);
		
		Sentence s1 = createSentence(1, completeWordList, 0, 2);
		Sentence s2 = createSentence(2, completeWordList2, 1, 3);
		
		List<Sentence> sentenceList = Stream.of(s1, s2).collect(Collectors.toList());

		
		when(sentenceDAO.select(1)).thenReturn(s1);
		when(sentenceDAO.selectAll()).thenReturn(sentenceList);

	}
	private static Word createWord(int id, String text, WordType type) {
		Word w = new Word();
		w.setId(id);
		w.setWord(text);
		w.setType(type);
		return w;
	}
	
	private static Sentence createSentence(int id, List<Word> wordList, int duplicates, int showDisplayCount) {
		Sentence sentence = new Sentence(new LinkedList<>(wordList));
		sentence.setId(id);
		sentence.setCreated(CREATED);
		sentence.setDuplicates(duplicates);
		sentence.setShowDisplayCount(showDisplayCount);
		return sentence;
	}

	private void dump(ResultActions resultActions) throws UnsupportedEncodingException {
		log.info(resultActions.andReturn().getResponse().getContentAsString());
	}
	
	@Test
	public void getWord() throws Exception {
		String RESPONSE = "{\"id\":10,\"word\":\"aaa\",\"type\":\"NOUN\"}";
		
		dump(mvc.perform(get("/words/aaa").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().json(RESPONSE, true)));
	}
	
	@Test
	public void wordDoesentExist() throws Exception {
		mvc.perform(get("/words/not").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().string(IsEmptyString.isEmptyOrNullString()));
	}
	
	
	
	@Test
	public void getAllWords() throws Exception {
		String RESPONSE = "[{\"id\":10,\"word\":\"aaa\",\"type\":\"NOUN\"},"
				+ "{\"id\":11,\"word\":\"adj\",\"type\":\"ADJECTIVE\"},"
				+ "{\"id\":12,\"word\":\"verb\",\"type\":\"VERB\"}]";
		
		dump(mvc.perform(get("/words").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().json(RESPONSE, true)));
	}
	
	@Test
	public void putWord() throws Exception {
		String content = "{\"type\": \"NOUN\"}";
		
		dump(mvc.perform(put("/words/bbb").contentType(MediaType.APPLICATION_JSON).content(content))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.word").value("bbb"))
		.andExpect(jsonPath("$.type").value(WordType.NOUN.name())));
	}

	
	@Test
	public void putWordOnForbidden() throws Exception {
		String content = "{\"type\": \"NOUN\"}";
		String error = "{\"errorMsg\": \"Illegal state: Forbidden word\"}";
		
		
		dump(mvc.perform(put("/words/AAA").contentType(MediaType.APPLICATION_JSON).content(content))
				.andExpect(status().isConflict())
				.andExpect(content().json(error, true)));
	}

	
	@Test
	public void getSentences() throws Exception {
		String RESPONSE = "[{\"id\":1,"
				+ "\"showDisplayCount\":2,"
				+ "\"created\":\"" + CREATED.getMillis() +"\","
				+ "\"duplicates\":0,"
				+ "\"text\":\"aaa verb adj.\"},"
				+ "{\"id\":2,"
				+ "\"showDisplayCount\":3,"
				+ "\"created\":\"" + CREATED.getMillis() +"\","
				+ "\"duplicates\":1,"
				+ "\"text\":\"NOUN VERB ADJECTIVE.\"}]";
		
		dump(mvc.perform(get("/sentences").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().json(RESPONSE, true)));
	}
	
	@Test
	public void getSentence() throws Exception {
		String RESPONSE = "{\"id\":1,"
				+ "\"showDisplayCount\":2,"
				+ "\"created\":\"" + CREATED.getMillis() +"\","
				+ "\"duplicates\":0,"
				+ "\"text\":\"aaa verb adj.\"}";
		
		dump(mvc.perform(get("/sentences/1").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().json(RESPONSE, true)));
	}
	
	@Test
	public void getSentenceyodaTalk() throws Exception {
		String RESPONSE = "{\"id\":1,"
				+ "\"showDisplayCount\":2,"
				+ "\"created\":\"" + CREATED.getMillis() +"\","
				+ "\"duplicates\":0,"
				+ "\"text\":\"aaa adj verb.\"}";
		
		dump(mvc.perform(get("/sentences/1/yodaTalk").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().json(RESPONSE, true)));
	}
	
	@Test
	public void generateWithoutWords() throws Exception {
		String RESPONSE = "{\"errorMsg\":\"Illegal state: There is no word with type: NOUN\"}";
		when(wordDAO.selectAll()).thenReturn(Collections.emptyList());
		
		dump(mvc.perform(post("/sentences/generate").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isConflict())
			.andExpect(content().json(RESPONSE, true)));
	}
	
	@Test
	public void generate() throws Exception {
		String RESPONSE = "{\"showDisplayCount\":0,"
				+ "\"duplicates\":0,"
				+ "\"text\":\"aaa verb adj.\"}";
		
		dump(mvc.perform(post("/sentences/generate").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isCreated())
			.andExpect(content().json(RESPONSE, false)));
	}
	
	
}
